import { Navigate, Outlet, useLocation,} from "react-router-dom";
import useAuth from "../hook/useAuth";


const RequireAuth = ({ roles }) => {
  const { auth } = useAuth();
const location=useLocation()
  console.log(auth);
  console.log(roles);
  return (
    <>
      {auth.roles?.find((role) => roles.includes(role)) ? (
        <Outlet />
      ) : auth?.user ? (
        <Navigate to="/unauthorized" state={{from:location}} replace />
      ) : (
        <Navigate to="/login" state={{from:location}} replace/>
      )}
    </>
  );
};

export default RequireAuth;
