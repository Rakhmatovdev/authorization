import React, { useEffect, useState } from "react";
import useAxiosPrivate from "../hook/useAxiosPrivate";

const User = () => {
  const [users, setUsers] = useState([]);
const privateAxios=useAxiosPrivate()
const [loading,setLoading]=useState(false)

  useEffect(() => {
    // let isMounted=true
    const controller = new AbortController();
    setLoading(true)
    const getUsers = async () => {
      try {
        const response = await privateAxios.get("/users", {
          signal: controller.signal,
        });
        setUsers(response?.data);
      } catch (error) {
        console.log(error);
      }
    };
    getUsers();
    setLoading(false)
    return () => {
      // cleanUp function
      controller.abort();
    };
  },[]);

if(loading){
  return <h2>Loading...</h2>
}

  return (
    <>
      <ul>

        {!users?.length ? (
          <h2>There is no user to show</h2>
        ) : (
          users?.map((user, index) => {
            return <li key={index}>{user?.username}</li>;
          })
        )}
      </ul>
      {/* <button onClick={refresh}>Refresh</button> */}

    </>
  );
};

export default User;
