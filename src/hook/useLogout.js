import useAuth from "./useAuth"
import axios from "../api/axios"

const useLogout = () => {
const {setAuth}=useAuth()

    const logOut= async()=>{
try {
    await axios.post('/logout',{withCredentials:true})
    setAuth({})
} catch (error) {
    console.log(error);
}
    }

  return logOut
}

export default useLogout