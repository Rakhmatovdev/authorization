import { useEffect, useState } from "react";
import useRefreshToken from "../hook/useRefreshToken";
import useAuth from "../hook/useAuth";
import { Outlet } from "react-router-dom";

const PersistLogin = () => {
  const refresh = useRefreshToken();
  const { auth, setAuth } = useAuth();
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    let isMounted = true;
    const verifyAccessToken = async () => {
      try {
        await refresh();
      } catch (error) {
        console.log(error);
      } finally {
        setLoading(false);
      }
    };
    isMounted && !auth?.accessToken ? verifyAccessToken() : setLoading(false);
    return () => (isMounted = false);
  }, [])
  return (
    <>
    {
loading ? <h2>Loading...</h2>:<Outlet />
} 
    </>
  );
};

export default PersistLogin;
