import axios from 'axios';

const BASE_URL='https://auth-refresh.onrender.com'


export default  axios.create({
    baseURL: BASE_URL
});

export const privateAxios=  axios.create({
    baseURL: BASE_URL,
    headers: {
        "Content-Type":"application/json",
      },
withCredentials:true
});