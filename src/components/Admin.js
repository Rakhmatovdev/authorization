import { Link } from "react-router-dom"
import User from "./User"

const Admin = () => {
    return (
        <section>
            <br />
            <h3>You must have been assigned an Admin role.</h3>
            <User/>
            <div className="flexGrow">
                <Link to="/">Home</Link>
            </div>
        </section>
    )
}

export default Admin
