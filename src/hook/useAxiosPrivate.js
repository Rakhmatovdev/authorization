import  { useEffect } from "react";
import useRefreshToken from "./useRefreshToken";
import useAuth from "./useAuth";
import { privateAxios } from "../api/axios";

const useAxiosPrivate = () => {
  const refresh = useRefreshToken();
  const { auth } = useAuth();
  useEffect(() => {
    const requestInterceptor= privateAxios.interceptors.request.use(
      (config) => {
        if (!config.headers["Authorization"]) {
          config.headers["Authorization"] = `Bearer ${auth?.accessToken}`;
        }
        return config;
      },
      (error) => {
        Promise.reject(error);
      }
    );

    const responseInterceptor=privateAxios.interceptors.response.use(
      (response) => response,
      async (error) => {
        const previouseReq=error?.config
        if (error?.response?.status === 403 && !previouseReq?._retry) {
            previouseReq._retry = true;
          const newAccessToken = await refresh();
          previouseReq.headers["Authorization"] = `Bearer ${newAccessToken}`;
          return privateAxios(previouseReq);
        } else{

             Promise.reject(error);
        }
      }
    );

    return () => {
      privateAxios.interceptors.request.eject(requestInterceptor);
      privateAxios.interceptors.response.eject(responseInterceptor);
    };
  }, [refresh, auth]);

  return privateAxios;
};

export default useAxiosPrivate;
