import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import AuthProvider from "./context/AuthProvider";
import { BrowserRouter, Route, Routes } from "react-router-dom";

ReactDOM.render(
  <AuthProvider>
    <BrowserRouter>
      <Routes>
        <Route path="*" element={<App/>}/>
      </Routes>
    </BrowserRouter>
  </AuthProvider>,
  document.getElementById("root")
);
